
// Mitchell Heyda Lab 03 
// This lab takes an input for the cost of a check, the number of people paying and the amount of tip each person wants to pay
//then calculates the final costs
import java.util.Scanner; //initializes scanner inputs
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in );
          System.out.print("Enter the original cost of the check in the form xx.xx: "); 
          double checkCost = myScanner.nextDouble(); //asks for the check cost and assigns it to a double
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
          double tipPercent = myScanner.nextDouble(); //asks for the amount of tip to pay and assigns it as a double
          tipPercent /= 100; //We want to convert the percentage into a decimal value
          System.out.print("Enter the number of people who went out to dinner: "); 
          int numPeople = myScanner.nextInt(); //assigns the number of people to an integer
          double totalCost;
          double costPerPerson;
          int dollars,   //whole dollar amount of cost 
              dimes, pennies;  //for storing digits to the right of decimal point for cost
          totalCost = checkCost * (1 + tipPercent); //calculates totalCost with tip 
          costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
          dollars=(int)costPerPerson;//get dimes amount, e.g., 
          // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
          //  where the % (mod) operator returns the remainder
          //  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10; //to output correct decimals
          pennies=(int)(costPerPerson * 100) % 10;
          System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
          
}  //end of main method   
  	} //end of class

