import java.util.*;

public class PlayLottery{

public static void main(String[] args) {
int winning[] = numbersPicked();
Scanner in =new Scanner(System.in);
int userNums[] = new int[5];
System.out.print("Enter 5 numbers between 0 and 59: ");

for(int i=0; i<5; i++)
  userNums[i] = in.nextInt();
  System.out.println("The user numbers are: "+Arrays.toString(userNums));
  System.out.println("The winning numbers are: "+Arrays.toString(winning));

if(userWins(userNums, winning))
  System.out.println("You win");

else
  System.out.println("You lose");

}

public static boolean userWins(int[] user, int[] winning){

for(int i=0; i<5; i++){
  if(user[i]!=winning[i])
    return false;
}
return true;
}

public static int[] numbersPicked(){
Random r = new Random();
int []nums = new int[5];
int n=0;

while(n<5){
  nums[n] = r.nextInt(59);
  boolean exists = false;
  for(int i=0; i<n; i++)
    if(nums[i]==nums[n]){
      exists = true;
      break;
}

  if(!exists)
    n++;
}
return nums;
}
}