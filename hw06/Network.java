
import java.util.InputMismatchException;
import java.util.Scanner;

public class Network {

	public static void main(String[] args) {
		int height, width, squareSize, edgeLength;
		Scanner scanner = new Scanner(System.in);
		int tmpLastRowStart = 0;
		
		while (true) {
			try {
				System.out.print("Input your desired heigth: ");
				height = scanner.nextInt();
				break;
			}catch(InputMismatchException exception) {
				System.out.println("\tError: please type in an integer.");

				scanner = new Scanner(System.in);

			}
		}
		while (true) {
			try {
				System.out.print("Input your desired width: ");

				width = scanner.nextInt();
				break;
			}catch(InputMismatchException exception) {
				System.out.println("\tError: please type in an integer.");
				scanner = new Scanner(System.in);
			}
		}
		while (true) {
			try {
				System.out.print("Input square size: ");

				squareSize = scanner.nextInt();
				break;
			}catch(InputMismatchException exception) {
				System.out.println("\tError: please type in an integer.");
				scanner = new Scanner(System.in);
			}
		}
		while (true) {
			try {
				System.out.print("Input edge length: ");

				edgeLength = scanner.nextInt();
				break;
			}catch(InputMismatchException exception) {
				System.out.println("\tError: please type in an integer.");
				scanner = new Scanner(System.in);
			}
		}
		
		int lastRowStart = 0;
		int lastColumnStart = 0;

			squareSize--;
		edgeLength++;

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (i == 0 || (lastRowStart + squareSize) == i) {
					// first row or height end of one box
					if (j == 0 || (lastColumnStart + squareSize) == j) {
						// first column or width end of one box
						System.out.print("#");
					}else if ((lastColumnStart + squareSize + edgeLength) == j) {
						// start of new one on width
						lastColumnStart = j;
						System.out.print("#");
					}else if (j > lastColumnStart && j < (lastColumnStart + squareSize)) {
						// box body on width
						System.out.print("-");
					}else {
						// width edge part
						if (squareSize == 0)  // if size is 1
							System.out.print("-");
						else
							System.out.print(" ");
					}
				}else if((lastRowStart + squareSize + edgeLength) == i) {
					// start of new one on height
					tmpLastRowStart = i;
					if (j == 0 || (lastColumnStart + squareSize) == j) {
						// first column or width end of one box
						System.out.print("#");
					}else if ((lastColumnStart + squareSize + edgeLength) == j) {
						// start of new one on width
						lastColumnStart = j;
						System.out.print("#");
					}else if (j > lastColumnStart && j < (lastColumnStart + squareSize)) {
						// box body on width
						System.out.print("-");
					}else {
						// width edge part

						if (squareSize == 0) { // if size is 1
								System.out.print("-");
						}else {
							System.out.print(" ");
						}

					}
					
				}else if(i > lastRowStart && i < (lastRowStart + squareSize)) {
					// box body on height
					if (j == 0 || (lastColumnStart + squareSize) == j) {
						// first column or width end of one box
						System.out.print("|");
					}else if ((lastColumnStart + squareSize + edgeLength) == j) {
						// start of new one on width
						lastColumnStart = j;
						System.out.print("|");
					}else if (j > lastColumnStart && j < (lastColumnStart + squareSize)) {
						// box body on width
						System.out.print(" ");
					}else {
						// width edge part
						if (i == (lastRowStart*2 + squareSize)/2) {
							System.out.print("-");
						}
						else if (squareSize % 2 == 1) {
							// even, double edge
							if (i == (lastRowStart*2 + squareSize)/2 + 1) {
								System.out.print("-");
							}else {
								System.out.print(" ");
							}
						}else {
							System.out.print(" ");
						}

					}
				}else {
					// height edge part
					if (j == (lastColumnStart*2 + squareSize)/2) {
						System.out.print("|");
					}
					else if (squareSize % 2 == 1) {
						// even, double edge
						if (j == (lastColumnStart*2 + squareSize)/2 + 1) {
							System.out.print("|");
						}else {
							if ((lastColumnStart + squareSize + edgeLength) == j)
								lastColumnStart = j;

								System.out.print(" ");
						}
					}else {
						if ((lastColumnStart + squareSize + edgeLength) == j)
							lastColumnStart = j;
						if (j == (lastColumnStart*2 + squareSize)/2  && squareSize == 0) 
							System.out.print("|");
						else
							System.out.print(" ");
					}
				}
			}
			lastColumnStart = 0;
			lastRowStart = tmpLastRowStart;
			System.out.println(); // new row
		}

		

	}

}
