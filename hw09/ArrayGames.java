//Mitchell Heyda
//Lab 09 asks users if they want to insert or shorten arrays, then a ranndom array is geerated and the specified function is performed.

import java.util.Random;
import java.util.Scanner;

public class ArrayGames {
	//Random object will be used for generation of all random values
	private Random rand;
	//Constructor will only instantiate Random object
	public ArrayGames() {
		rand = new Random();
	}

	//Method which will generate array of 10 - 20 integers
	public int[] generate() {
		//maximum value for array values, not stated in assignment, can be changed
		int maxValue = 100;
		//size will be random number 0-10
		int size = rand.nextInt(11);
		//add offset so that size of the array is in the range 10-20
		size += 10;
		//now size is known, create the array
		int array[] = new int[size];
		//and set all values of the array to random numbers 0-99
		for (int i = 0; i < size; i++) {
			array[i] = rand.nextInt(maxValue);
		}
		//return created array
		return array;
	}
	
	//print out array passed as argument in given format
	public void print (int[] array) {
		//index will be incremented as we iterate through the array
		int index = 0;
		//print open bracket (note that print not println is used)
		System.out.print("{");
		//one after the other, print all array elements, with comma between
		//except for the first one where comma is not needed
		for (int i : array) {
			if (index != 0)
				System.out.print(",");
			index++;
			System.out.print(i);
		}
		//At the end, close the curly bracket in printout
		System.out.print("}");
	}
	
	//insert method will insert elements of array2 into random position
	//within array1
	public int[] insert(int[] array1, int[] array2) {
		//Print information about the method that is executed
		System.out.println("Running insert method");
		//Print input arguments in desired format
		System.out.print("Input 1:");
		print(array1);
		System.out.print(" Input 2:");
		print(array2);
		//size of the output array will be equal to the sum of array1 and array2 sizes
		int arraySize = array1.length + array2.length;
		int array[] = new int[arraySize];
		//determine random position where to insert elements from array2
		int position = rand.nextInt(array1.length);
		//up to that position, copy elements from array1
		for(int i = 0; i < position; i++) {
			array[i] = array1[i];
		}
		//then insert elements from array2
		for(int i = 0; i < array2.length; i++) {
			array[position + i] = array2[i];
		}
		//at the end, continue with remaining elements from array1
		for(int i = 0; i < array1.length - position; i++) {
			array[position+array2.length+i] = array1[position+i];
		}
		//Print out the output, as required
		System.out.println();
		System.out.print("Output:");
		print(array);

		//return output array
		return array;					
	}
	
	//shorten method will remove one element from input array
	public int[] shorten(int[] array, int remInd) {
		//Print information similarly to insert method
		System.out.println("Running shorten method");
		System.out.print("Input 1:");
		print(array);
		System.out.print(" Input 2:"+remInd);

		//if remInd is not a valid index in input array, return
		//array unchanged and also print it out as requested
		if (remInd >= array.length){
			System.out.println();
			System.out.print("Output:");
			print(array);
			return array;
		}
		
		//otherwise, create new array which will contain 
		//one element less than the original array
		int retArray[] = new int[array.length - 1];
		//local variables that will be used to iterate 
		//through output and input arrays
		int ind = 0;
		int i = 0;
		//iterate over complete input array
		while (i < array.length) {
			//and store all elements but the one at index remInd to 
			//output array
			if (i != remInd)
				retArray[ind++] = array[i];
			i++;
		}
		//Print out the resulting array
		System.out.println();
		System.out.print("Output:");
		print(retArray);
		
		//return output array
		return retArray;
	}
	
	//main method
	public static void main(String[] args) {
		//scanner is used to input value
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		//create object of HandlingArrays class
		ArrayGames ha = new ArrayGames();
		//generate required arrays and numbers that will be used by either 
		//insert or shorten method
		int a[] = ha.generate();
		int b[] = ha.generate();
		int remInd = ha.rand.nextInt(a.length);
		//continue asking user to input 1 or 2 until he inputs 1 or 2
		while (choice != 1 && choice != 2) {
			System.out.print("Please choose (1) for insert of (2) for shorten:");
			choice = sc.nextInt();
			//depending on the choice by the user, run either insert
			if (choice == 1)
				ha.insert(a, b);
			if (choice == 2)
				//or shorten method
				ha.shorten(a, remInd);
		}

	}

}
