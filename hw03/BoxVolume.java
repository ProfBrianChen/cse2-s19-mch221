//Mitchell Heyda HW 03 box volume 2/11/19
//this takes inputs from width length and height then multiplies them to be a volume and outputs it
import java.util.Scanner; //sets up inputs
public class BoxVolume {
    	// main method required for every Java program
   	public static void main(String[] args) {
   
      //prompts for width and assigns it to a double 
      Scanner myScanner = new Scanner( System.in ); 
      System.out.print("The width of the box is: ");
      double width = myScanner.nextDouble();
  
      //prompts for width and assigns it to a double 
    
      System.out.print("The length of the box is: ");
      double length = myScanner.nextDouble();
  
      //prompts for width and assigns it to a double 

      System.out.print("The height of the box is: ");
      double height = myScanner.nextDouble();
      
      //Calculates volume and assigns it to a double 
      double volume = width * length * height;
      System.out.println("The volume of the box is: " + volume);
      
      
	}  //end of main method   
} //end of class

