//Mitchell Heyda HW03 Convert 2/11/19
//This program inputs a distance in meters and converts it into inches

import java.util.Scanner; //sets up inputs
public class Convert {
    	// main method required for every Java program
   	public static void main(String[] args) {
   
      //prompts for input and assigns it to a double 
      Scanner myScanner = new Scanner( System.in ); 
      System.out.print("Enter the distance in meters: ");
      double meters = myScanner.nextDouble();
    
      //conversion factor for meters to inches
      double inches = meters * 39.3701;
      
      //outputs final answers and format
      System.out.println(meters + " meters is " + inches + " inches.");
      System.out.print("");

	}  //end of main method   
} //end of class

