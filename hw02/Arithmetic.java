//Mitchell Heyda Hw02 Arithmetic
//This program takes inputted numbers of different clothing items as well as their prices
//it then converts this information into total cost of each item, taxes on each item, total tax, and final price with and without tax

public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {
//-------------Prices and # of items ------------------------------------------------------------
       //Number of pairs of pants
         int numPants = 3;     
       //Cost per pair of pants
         double pantsPrice = 34.98;
       //Number of sweatshirts
         int numShirts = 2;
       //Cost per shirt
         double shirtPrice = 24.99;
       //Number of belts
         int numBelts = 1;
       //cost per belt
         double beltCost = 33.99;
       //the tax rate
         double paSalesTax = 0.06;
//--------------Individul Price ------------------------------------------------- ---------------      
       //total cost of pants
         double totalPantsCost = numPants * pantsPrice;
       //total cost of shirts
         double totalShirtsCost = numShirts * shirtPrice;
       //total cost of belts
         double totalBeltsCost = numBelts * beltCost;
//----------------TAXES-----------------------------------------------------------------------------------------------------       
       //Sales tax on pant
         double pantsTax = (int) ((totalPantsCost * paSalesTax) * 100) / 100.00;
       //sales tax on shirts
         double shirtsTax = (int) ((totalShirtsCost * paSalesTax) * 100) / 100.0;
       //sales tax on belts
         double beltsTax = (int) ((totalBeltsCost * paSalesTax) * 100) / 100.0;
       //total sales tax
        double totalTax = pantsTax + shirtsTax + beltsTax;
//--------------TOTALS------------------------------------------------------------------
       //total cost w/o tax
         double totalCost = totalBeltsCost + totalShirtsCost +totalPantsCost;
       //total cost w/ tax - total for transaction
         double finalCost = totalCost + totalTax;
      
      
      //Prints total prices of all different items
      System.out.println("");
      System.out.println("-----------Cost of Individual Items----------- ");
      System.out.println("");
      System.out.println("Cost of Pants: " + (totalPantsCost));
      System.out.println("Cost of Shirts: " + (totalShirtsCost));
      System.out.println("Cost of Betls: " + (totalBeltsCost));
      System.out.println("");
      //prints all taxes on each item bought
      System.out.println("------------Taxes on each Item --------------- ");
      System.out.println("");
      System.out.println("Tax on all pants: " + (pantsTax));
      System.out.println("Tax on all shirts: " + (shirtsTax));
      System.out.println("Tax on all belts: " + (beltsTax));
      System.out.println("");
      //total sales tax
      System.out.println("------------Totals ---------------------------- ");
      System.out.println("");
      System.out.println("Total sales tax : " + (totalTax));
    
      //Prints total cost of all items before tax
      System.out.println("Total cost before tax: " + (totalCost));
      
      //Prints out final total cost w/ tax
      System.out.println("Total cost with tax / Final Cost: " + (finalCost));
      System.out.println("");
      System.out.println("-------------------------------------------------");
      
	}  //end of main method   
} //end of class

