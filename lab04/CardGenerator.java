//Mitchell Heyda
//
import java.util.Random;
public class CardGenerator {
public static void main(String[] args){

    int card = 0;
    int suit = 0;
    int value = 0;
    String suitName = "";
    String valueName = "";
    
    //creates random number generator from 1-52
    Random generator = new Random();
    card = generator.nextInt(52) + 1;
    
    //divides the card number by 13 to get a nnumber from 0 to 4
    suit = card / 13;
    //if value is between 1-13 remainder will be 0, 13-26 will be 1, etc. These if statements assign a suit name to each product
    if (suit == 0 ){suitName = "Hearts";}
    else if (suit == 1){suitName = "Clubs";}
    else if (suit == 2){suitName = "Diamonds";}
    else if (suit == 3){suitName = "Spades";}
  
    //Finds the remainder of the card number and 13 to assign the names of each value (1- 10, Jack ,queen, king, ace)
    value = card % 13;
    //Switch assigns a valueName to each possible 1-13 value of value
    switch (value){
      case 0: valueName = "King";
              break;
      case 1: valueName = "Ace";
              break;
      case 2: valueName = "2";
              break;
      case 3: valueName = "3";
              break;
      case 4: valueName = "4";
              break;
      case 5: valueName = "5";
              break;
      case 6: valueName = "6";
              break;
      case 7: valueName = "7";
              break;
      case 8: valueName = "8";
              break;
      case 9: valueName = "9";
              break;
      case 10: valueName = "10";
              break;
      case 11: valueName = "Jack";
              break;
      case 12: valueName = "Queen";
              break;
    }
      
  
  System.out.println("Your Card is the: "+ valueName +" of "+ suitName);

                            
  
}
}