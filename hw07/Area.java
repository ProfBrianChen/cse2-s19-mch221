//Mitchell Heyda hw07 - 3/26
//This program takes in a strig of shaoe name as well as the parameters of that shape then finds the area
//
import java.util.*;
public class Area{
   public static void main(String args[]){
       String shape;
       int h = 0;
       double length,breadth,radius,base,height;
       System.out.print("Enter a shape (rectangle, triangle or a circle) :");
       Scanner sc = new Scanner(System.in);
       do{
           h = 0;
           shape = sc.next();
           if(shape.equals("rectangle")){
               System.out.print("Enter a length : ");
               length = sc.nextDouble();
               System.out.print("Enter a width : ");
               breadth = sc.nextDouble();
               if(length >= 0 && breadth >= 0){
                   Area.rectangle(length,breadth);
               }else{
                   System.out.println("\nInvalid inputs.please retry");
               }
           }else if(shape.equals("triangle")){
               System.out.print("Enter a height : ");
               height = sc.nextDouble();
               System.out.print("Enter a base : ");
               base = sc.nextDouble();
               if(height >= 0 && base >= 0){
                   Area.triangle(height,base);
               }else{
                   System.out.println("\nInvalid inputs.please retry");
               }
           }else if(shape.equals("circle")){
               System.out.print("Enter a radius : ");
               radius = sc.nextDouble();
               if(radius >= 0 ){
                   Area.circle(radius);
               }else{
                   System.out.println("\nInvalid inputs.please retry");
               }
           }else{
               System.out.print("\nEnter valid shape (rectangle or triangle or circle) : ");
               h = 1;
           }
       }while (h != 0);
   }
   public static void rectangle(double length,double breadth){
       System.out.println("\nArea of the rectangle : " + length*breadth);
   }
   public static void triangle(double height,double base){
       System.out.println("\nArea of the triangle : " + 0.5*height*base);
   }
   public static void circle(double radius){
       System.out.println("\nArea of the circle : " + 3.14*radius*radius);
   }
}

