//Mitchell Heyda
//This program generates 5 different cards then compares the cards to tell if there is one pair, two pairs, or three of a kind present in the 5 card hand
import java.util.Random; //imports the random number generator
public class PokerHandCheck {
public static void main(String[] args){

    int card1  = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;
    int suit1 = 0;
    int suit2 = 0;
    int suit3 = 0;
    int suit4 = 0;
    int suit5 = 0;
    int value1 = 0;
    int value2 = 0;
    int value3 = 0;
    int value4 = 0;
    int value5 = 0;
    String suitName1 = "";
    String suitName2 = "";
    String suitName3 = "";
    String suitName4 = "";
    String suitName5 = "";
    String valueName1 = "";
    String valueName2 = "";
    String valueName3 = "";
    String valueName4 = "";
    String valueName5 = "";
    
    //creates random number generator from 1-52 for 5 cards
    Random generator = new Random();
    card1 = generator.nextInt(52) + 1;
    card2 = generator.nextInt(52) + 1;
    card3 = generator.nextInt(52) + 1;
    card4 = generator.nextInt(52) + 1;
    card5 = generator.nextInt(52) + 1;
    
    //divides the card number by 13 to get a nnumber from 0 to 4 for card 1-5
    suit1 = card1 / 13;
    suit2 = card2 / 13;
    suit3 = card3 / 13;
    suit4 = card4 / 13;
    suit5 = card5 / 13;
    //if value is between 1-13 remainder will be 0, 13-26 will be 1, etc. These if statements assign a suit name to each product for card 1-5
    if (suit1 == 0 ){suitName1 = "Hearts";}
    else if (suit1 == 1){suitName1 = "Clubs";}
    else if (suit1 == 2){suitName1 = "Diamonds";}
    else if (suit1 >= 3){suitName1 = "Spades";}
  
    if (suit2 == 0 ){suitName2 = "Hearts";}
    else if (suit2 == 1){suitName2 = "Clubs";}
    else if (suit2 == 2){suitName2 = "Diamonds";}
    else if (suit2 >= 3){suitName2 = "Spades";}
  
    if (suit3 == 0 ){suitName3 = "Hearts";}
    else if (suit3 == 1){suitName3 = "Clubs";}
    else if (suit3 == 2){suitName3 = "Diamonds";}
    else if (suit3 >= 3){suitName3 = "Spades";}
  
    if (suit4 == 0 ){suitName4 = "Hearts";}
    else if (suit4 == 1){suitName4 = "Clubs";}
    else if (suit4 == 2){suitName4 = "Diamonds";}
    else if (suit4 >= 3){suitName4 = "Spades";}
  
    if (suit5 == 0 ){suitName5 = "Hearts";}
    else if (suit5 == 1){suitName5 = "Clubs";}
    else if (suit5 == 2){suitName5 = "Diamonds";}
    else if (suit5 >= 3){suitName5 = "Spades";}
  
    //Finds the remainder of the card number and 13 to assign the names of each value (1- 10, Jack ,queen, king, ace) for card 1-5
    value1 = card1 % 13;
    value2 = card2 % 13;
    value3 = card3 % 13;
    value4 = card4 % 13;
    value5 = card5 % 13;
    //Switch assigns a valueName to each possible 1-13 value of value for card 1-5
    switch (value1){
      case 0: valueName1 = "King";
              break;
      case 1: valueName1 = "Ace";
              break;
      case 2: valueName1 = "2";
              break;
      case 3: valueName1 = "3";
              break;
      case 4: valueName1 = "4";
              break;
      case 5: valueName1 = "5";
              break;
      case 6: valueName1 = "6";
              break;
      case 7: valueName1 = "7";
              break;
      case 8: valueName1 = "8";
              break;
      case 9: valueName1 = "9";
              break;
      case 10: valueName1 = "10";
              break;
      case 11: valueName1 = "Jack";
              break;
      case 12: valueName1 = "Queen";
              break;
    }    
    switch (value2){
      case 0: valueName2 = "King";
              break;
      case 1: valueName2 = "Ace";
              break;
      case 2: valueName2 = "2";
              break;
      case 3: valueName2 = "3";
              break;
      case 4: valueName2 = "4";
              break;
      case 5: valueName2 = "5";
              break;
      case 6: valueName2 = "6";
              break;
      case 7: valueName2 = "7";
              break;
      case 8: valueName2 = "8";
              break;
      case 9: valueName2 = "9";
              break;
      case 10: valueName2 = "10";
              break;
      case 11: valueName2 = "Jack";
              break;
      case 12: valueName2 = "Queen";
              break;
    }    
    switch (value3){
      case 0: valueName3 = "King";
              break;
      case 1: valueName3 = "Ace";
              break;
      case 2: valueName3 = "2";
              break;
      case 3: valueName3 = "3";
              break;
      case 4: valueName3 = "4";
              break;
      case 5: valueName3 = "5";
              break;
      case 6: valueName3 = "6";
              break;
      case 7: valueName3 = "7";
              break;
      case 8: valueName3 = "8";
              break;
      case 9: valueName3 = "9";
              break;
      case 10: valueName3 = "10";
              break;
      case 11: valueName3 = "Jack";
              break;
      case 12: valueName3 = "Queen";
              break;
    }    
    switch (value4){
      case 0: valueName4 = "King";
              break;
      case 1: valueName4 = "Ace";
              break;
      case 2: valueName4 = "2";
              break;
      case 3: valueName4 = "3";
              break;
      case 4: valueName4 = "4";
              break;
      case 5: valueName4 = "5";
              break;
      case 6: valueName4 = "6";
              break;
      case 7: valueName4 = "7";
              break;
      case 8: valueName4 = "8";
              break;
      case 9: valueName4 = "9";
              break;
      case 10: valueName4 = "10";
              break;
      case 11: valueName4 = "Jack";
              break;
      case 12: valueName4 = "Queen";
              break;
    }    
    switch (value5){
      case 0: valueName5 = "King";
              break;
      case 1: valueName5 = "Ace";
              break;
      case 2: valueName5 = "2";
              break;
      case 3: valueName5 = "3";
              break;
      case 4: valueName5 = "4";
              break;
      case 5: valueName5 = "5";
              break;
      case 6: valueName5 = "6";
              break;
      case 7: valueName5 = "7";
              break;
      case 8: valueName5 = "8";
              break;
      case 9: valueName5 = "9";
              break;
      case 10: valueName5 = "10";
              break;
      case 11: valueName5 = "Jack";
              break;
      case 12: valueName5 = "Queen";
              break;
    }    
  
  //checks for one pair in the 5 cards --> 5 choose 2
  boolean isPair;
  if (valueName1 == valueName2 ||valueName1 == valueName3||valueName1 == valueName4||valueName1 == valueName5
    ||valueName2 == valueName3||valueName2 == valueName4||valueName2 == valueName5
    ||valueName3 == valueName4||valueName3 == valueName5
    ||valueName4 == valueName5){
    isPair = true;
    
  }
  else {
    isPair = false;
  }
  
  //looks for three of a kind in the 5 cards --> 5 choose 3
  boolean isThreeKind;
  if (valueName1 == valueName2 && valueName1 == valueName3 || valueName1 == valueName2 && valueName1 == valueName4 || valueName1 == valueName2 && valueName1 == valueName5
     ||valueName1 == valueName3 && valueName1 == valueName4 || valueName1 == valueName3 && valueName1 == valueName5
     ||valueName1 == valueName4 && valueName1 == valueName5
     ||valueName2 == valueName3 && valueName2 == valueName4||valueName2 == valueName3 && valueName2 == valueName5
     ||valueName2 == valueName4 && valueName2 == valueName5
     ||valueName3 == valueName4 && valueName3 == valueName5){
    isThreeKind = true;
  }
  else {
    isThreeKind = false;
  }
  
  //creates a boolean variable for each instance of a pair possible
  boolean v1Pair;
  if (valueName1 == valueName2 || valueName1 == valueName3 || valueName1 == valueName4 || valueName1 == valueName5){v1Pair = true;}
  else {v1Pair = false;}
  
  boolean v2Pair;
  if (valueName2 == valueName1|| valueName2 == valueName3|| valueName2 == valueName4 || valueName2 == valueName5){v2Pair = true;}
  else {v2Pair = false;}
  
  boolean v3Pair;
  if (valueName3 == valueName1 || valueName3 == valueName2 || valueName3 == valueName4 || valueName3 == valueName5){v3Pair = true;}
  else {v3Pair = false;}
  
  boolean v4Pair;
  if (valueName4 == valueName1 || valueName4 == valueName2 || valueName4 == valueName3 || valueName4 == valueName5){v4Pair = true;}
  else {v4Pair = false;}
  
  boolean v5Pair;
  if (valueName5 == valueName1|| valueName5 == valueName2|| valueName5 == valueName3 || valueName5 == valueName4){v5Pair = true;}
  else {v5Pair = false;}
  
  //when 4 of the above statements are true, then there are 2 pairs
  //because v1 = v2 , v2=v1 etc.
  boolean twoPair;
  if ((v1Pair == true && v2Pair == true && v3Pair == true && v4Pair == true)||(v1Pair == true && v3Pair == true && v4Pair == true && v5Pair ==true)||(v2Pair == true && v3Pair == true && v4Pair== true && v5Pair==true)){twoPair = true;}
  else {twoPair = false;}
  
  //Prints the values of each card picked
  System.out.println("Your random cards were: ");
  System.out.println(valueName1 +" of "+ suitName1);
  System.out.println(valueName2 +" of "+ suitName2);
  System.out.println(valueName3 +" of "+ suitName3);
  System.out.println(valueName4 +" of "+ suitName4);
  System.out.println(valueName5 +" of "+ suitName5);
  System.out.println("");
  
  //prints the number of pairs, three of a kind, or if you have a high card hand.
  if (twoPair == true){
    System.out.println("You have two pairs");
  }
  
  else if (isThreeKind == true){
    System.out.println("You have three of a kind");
  }
  else if (isPair == true){
    System.out.println("You have a pair");
  }
  else {
    System.out.println("You have a high card hand!");
  }
}
}