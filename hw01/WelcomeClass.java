///////////////
//// CSE 02 Hello Word
//// MItchell Heyda
public class WelcomeClass{
  
  public static void main (String args[]){
    ///Prints Hello, World to terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--C--H--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
  
}