
// Mitchell Heyda Lab 02 
//This lab takes input in seconds and numbers of wheel rotations to calculate distance traveled
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
// our input data. Document your variables by placing your
// comments after the “//”. State the purpose of each variable.
int secsTrip1=480;  //time of trip 1 in seconds
int secsTrip2=3220;  //time of trip 2 in seconds
int countsTrip1=1561;  //# of cycloroations trip 1
int countsTrip2=9037; //trip 2 rotations

// our intermediate variables and output data. Document!
double wheelDiameter=27.0,  //size of the wheel
PI=3.14159, // pi as a conversion from # rotation to distance
feetPerMile=5280,  // conversion factor
inchesPerFoot=12,   //conversion factor
secondsPerMinute=60;  //conversion factor
double distanceTrip1, distanceTrip2,totalDistance;  //defines these variables

      //print out the numbers that you have stored in the variables that store number of seconds (converted to minutes) and the counts
      System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1+" counts."); 
      System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute)+ " minutes and had " + countsTrip2+" counts.");


      //run the calculations; store the values.
      //This calculates distance in inches 
      distanceTrip1=countsTrip1*wheelDiameter*PI;
      // Above gives distance in inches
      //(for each count, a rotation of the wheel travels
      //the diameter in inches times PI)
      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //same calculations as above but for trip 2
      totalDistance=distanceTrip1+distanceTrip2; //calculates total distance

      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
      System.out.println("Trip 2 was "+distanceTrip2+" miles");
      System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class
