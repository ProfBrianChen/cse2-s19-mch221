//Mitchell Heyda
//Lab 05 
//This program asks the user to input length as an integer
//if input is not an integer, or is negative, the program asks the user for another input
//Program then outputs a "twist" pattern as long as the length the user inputted.
import java.util.Scanner; //imports scanner

public class TwistGenerator {
public static void main(String[] args){ 

  Scanner input = new Scanner( System.in );
  int check = 0;
  
  while(check == 0){
       System.out.print("Enter a postive integer for length: ");
     
       if(input.hasNextInt()){
          int length = input.nextInt();
               
           if(length > 0){
              check++;
              int x = (length - 1) / 3;
              int slashes = length;
              int c1 = 0;
              int c2 = 0;
              int c3 = 0;
              
             while(c1 < slashes){ //for line 1
                   System.out.print("\\ ");
                   c1+=2; //prints the correct foreward slash in the correct spot, adds 2
                       
                if(c1 < slashes){
                    System.out.print("/");
                    c1++;} //prints the correct backslack w/ right spacing, adds 1
                             }
                  
         System.out.println("");//spaces for formatting
                    
         while(c2 < x ){ //for line 2
               System.out.print(" X ");
               c2++; } //prints crosses in the correct place 
                   
         System.out.println("");//spaces for formatting
                 
         while(c3 < slashes ){ //for line 3
               System.out.print("/ ");
               c3+=2; //prints foreslash on the bottom line in correct spot, adds 2
            if (c3 < slashes){
               System.out.print("\\"); //prints backslash in bottom in the same spot, adds 1 
               c3++; }
                         }
             }
               
         else{System.out.println("not a positive integer");} //if input is not positive returns to the start of the loop
           
       }
            else{System.out.println("not an integer"); //if input is not an integer it returns to the start of the loop
                input.next();} }  

  System.out.println("");//spaces for formatting

}

}

